# NSS planner


| Completed | Location |
| ------ | ------ |
| **Use Cases** |
| Getting last weeks tasks | getLastWeeksTasks in TaskService class |
| Closing a Task | closeTask in TaskService class |
| Users have a rating for each completed task | closeTask in TaskService |
| Creating a Task | createTask in TaskService|
| Creating a User | registration in UserService|
| Unsubscribe from a Task| unsubscribe in TaskService|
| **Design Patterns** |
| Proxy | Implemented in UserProxy class |
| Facade | Implemented in UserService class |
| Decorator | Implemented in AdminServiceDecorator |
| Observer | Implemented in UserService |
| Interceptor | Implemented in UserServiceInterceptor|
| **Technologies** | |
| Kafka | Described in application.yml file |
| Basic Auth | Located in WebSecurityConfig |
| Interceptor | Implemented in UserServiceInterceptor |
| Rest | Classes located in the controller package |
| Event Based Architecture | Simulated in Monolith |
| Responsive Design| vue.js |


| Failed to implement | 
| ------ | 
| **Technologies** | 
| elastic  | 
| heroku |

| Role | Username | Password |
| ------ | ------ | ------ |
| Manager | format | 1234 |
| User | krcilja | 1234 |

[Information Document](https://docs.google.com/document/d/1d8itb5gOnL8MxGJxwC8FjdiUU7cyF4q_xPhhOnaIcnY/edit)

