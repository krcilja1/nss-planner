package cz.cvut.fel.nss.controller;


import cz.cvut.fel.nss.dto.IdDto;
import cz.cvut.fel.nss.dto.UserDto;
import cz.cvut.fel.nss.model.User;
import cz.cvut.fel.nss.service.ManagerService;
import cz.cvut.fel.nss.service.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/manager")
public class ManagerController {

    @Autowired
    private ManagerService managerService;

    @Autowired
    private ProducerService kafkaProducer;

    private static final String TOPIC = "manager-approve";


    @PostMapping("/approve")
    public void approveUser(@RequestBody IdDto id){
        kafkaProducer.sendMessage(Integer.toString(id.getId()), TOPIC);
        //managerService.approveUser(id);
    }

    @GetMapping("/getAll")
    public List<UserDto> listAll() {
        List<User> users = managerService.findAll();
        List<UserDto> result = new ArrayList<>();
        for(User u : users){
            result.add(u.convertToDto());
        }
        return result;
    }
}
