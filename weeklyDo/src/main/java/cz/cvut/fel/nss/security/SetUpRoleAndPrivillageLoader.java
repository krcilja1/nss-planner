package cz.cvut.fel.nss.security;

import cz.cvut.fel.nss.UserProxy;
import cz.cvut.fel.nss.dao.PrivilegeDao;
import cz.cvut.fel.nss.dao.RoleDao;
import cz.cvut.fel.nss.dao.UserDao;
import cz.cvut.fel.nss.model.Privilege;
import cz.cvut.fel.nss.model.Role;
import cz.cvut.fel.nss.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.transaction.Transactional;
import java.util.*;

public class SetUpRoleAndPrivillageLoader implements
        ApplicationListener<ContextRefreshedEvent> {

        boolean alreadySetup = false;

@Autowired
private UserDao userRepository;

@Autowired
private RoleDao roleRepository;

@Autowired
private PrivilegeDao privilegeDao;

@Autowired
private PasswordEncoder passwordEncoder;

@Override
@Transactional
public void onApplicationEvent(ContextRefreshedEvent event) {

        if (alreadySetup)
        return;
        Privilege readPrivilege
        = createPrivilegeIfNotFound("READ_PRIVILEGE");
        Privilege writePrivilege
        = createPrivilegeIfNotFound("WRITE_PRIVILEGE");

        List<Privilege> adminPrivileges = Arrays.asList(
        readPrivilege, writePrivilege);
        createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
        createRoleIfNotFound("ROLE_USER", Arrays.asList(readPrivilege));

        Role adminRole = roleRepository.findByName("ROLE_ADMIN").get(0);
        UserProxy user = new UserProxy(new User());
        user.setFirstName("Test");
        user.setLastName("Test");
        user.setPassword(passwordEncoder.encode("test"));
        user.setEmail("test@test.com");
        Set<Role> roles = new HashSet<>();
        roles.add(adminRole);
        user.setRoles(roles);
      //  user.setEnabled(true);
        userRepository.save(user.getUser());

        alreadySetup = true;
        }

@Transactional
    Privilege createPrivilegeIfNotFound(String name) {

            Privilege privilege = privilegeDao.findByName(name).get(0);
            if (privilege == null) {
            privilege = new Privilege(name);
                privilegeDao.save(privilege);
            }
            return privilege;
            }

@Transactional
    Role createRoleIfNotFound(
            String name, Collection<Privilege> privileges) {

        Role role = roleRepository.findByName(name).get(0);
        if (role == null) {
        role = new Role(name);
        role.setPrivileges(privileges);
        roleRepository.save(role);
        }
        return role;
        }
}