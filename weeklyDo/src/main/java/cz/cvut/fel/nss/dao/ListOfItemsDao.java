package cz.cvut.fel.nss.dao;

import cz.cvut.fel.nss.model.ListOfItems;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ListOfItemsDao extends JpaRepository<ListOfItems, Integer> {
}
