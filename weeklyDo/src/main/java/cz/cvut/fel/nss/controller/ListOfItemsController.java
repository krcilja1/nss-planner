package cz.cvut.fel.nss.controller;

import cz.cvut.fel.nss.dao.ListOfItemsDao;
import cz.cvut.fel.nss.dto.ListOfItemsDto;
import cz.cvut.fel.nss.model.ListOfItems;
import cz.cvut.fel.nss.service.ListOfItemsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
public class ListOfItemsController {
    @Autowired
    private ListOfItemsService listOfItemsService;

    @GetMapping("/listOfItems")
    public List<ListOfItemsDto> listAll() {
        List<ListOfItems> listOfItems = listOfItemsService.findAll();
        List<ListOfItemsDto> result = new ArrayList<>();
        for(ListOfItems list: listOfItems){
            result.add(
                    new ListOfItemsDto(
                            String.valueOf(list.getId()),
                            list.getName(),
                            String.valueOf(list.getUser().getId())
                    )
            );
        }

        return result;
    }

    @PostMapping("/createListOfItems")
    public void createListOfItems(@RequestBody ListOfItemsDto dto){
        listOfItemsService.createListOfItems(dto);
    }
    @PostMapping("/updateListOfItems")
    public void updateListOfItems(@RequestBody ListOfItemsDto dto){
        listOfItemsService.updateListOfItems(dto);
    }
    @PostMapping("/deleteListOfItems")
    public void deleteListOfItems(@RequestParam int id){
        listOfItemsService.deleteListOfItems(id);
    }
    @GetMapping("/getListOfItems")
    public ListOfItemsDto getListOfItems(@RequestParam int id){
        return listOfItemsService.getListOfItems(id);
    }

}