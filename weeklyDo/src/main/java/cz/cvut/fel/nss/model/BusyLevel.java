package cz.cvut.fel.nss.model;

public enum BusyLevel {
    VERYBUSY("STATE_VERYBUSY"), BUSY("STATE_BUSY"), ALMOSTFREE("STATE_ALMOSTFREE"), FREE("STATE_FREE");

    private final String name;

    BusyLevel(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}

