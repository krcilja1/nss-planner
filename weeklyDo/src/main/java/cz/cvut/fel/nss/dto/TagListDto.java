package cz.cvut.fel.nss.dto;

import cz.cvut.fel.nss.model.Tag;

import java.util.List;

public class TagListDto {
    private List<Integer> tags;

    public TagListDto(){
    }

    public TagListDto(List<Integer> tags) {
        this.tags = tags;
    }

    public List<Integer> getTags() {
        return tags;
    }

    public void setTags(List<Integer> tags) {
        this.tags = tags;
    }
}
