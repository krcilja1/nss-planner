package cz.cvut.fel.nss.dto;

import java.util.Date;

public class NotificationDto {
    private Date dateCreated;
    private String name;

    public NotificationDto(Date dateCreated, String name) {
        this.dateCreated = dateCreated;
        this.name = name;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
