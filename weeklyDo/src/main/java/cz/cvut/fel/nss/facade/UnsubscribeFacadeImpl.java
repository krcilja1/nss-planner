package cz.cvut.fel.nss.facade;

import cz.cvut.fel.nss.controller.UserController;
import cz.cvut.fel.nss.dto.TaskDto;
import cz.cvut.fel.nss.model.Task;
import cz.cvut.fel.nss.service.TaskService;
import cz.cvut.fel.nss.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UnsubscribeFacadeImpl implements UnsubscribeFacade {

    @Autowired
    UserService userService;
    @Autowired
    TaskService taskService;

    @Override
    public void Unsubscribe(String username, TaskDto task) {
        Task temp = taskService.findByName(task.getName());
        userService.unsubscribeTask(username,temp);
    }
}
