package cz.cvut.fel.nss.controller;

import cz.cvut.fel.nss.dao.TagDao;
import cz.cvut.fel.nss.dao.TaskDao;
import cz.cvut.fel.nss.dto.TagDto;
import cz.cvut.fel.nss.dto.TagTaskIdDto;
import cz.cvut.fel.nss.model.Tag;
import cz.cvut.fel.nss.model.Task;
import cz.cvut.fel.nss.service.ProducerService;
import cz.cvut.fel.nss.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/user")
public class TagController {


    @Autowired
    private TagService tagService;
    @Autowired
    private ProducerService kafkaProducer;

    private static final String ADD_TOPIC = "add-tag-task";
    private static final String REMOVE_TOPIC = "remove-tag-task";
    private static final String NEW_TOPIC = "new-tag-task";

    @PostMapping("/addTagTask")
    public void addTask(@RequestBody TagTaskIdDto dto) {
        //tagService.addTask(dto.getTagId(),dto.getTaskId());
        kafkaProducer.sendMessage(Integer.toString(dto.getTagId()) + " " + Integer.toString(dto.getTaskId()), ADD_TOPIC);
    }

    @PostMapping("/removeTask")
    public void removeTask(@RequestBody TagTaskIdDto dto) {
        //tagService.removeTask(dto.getTagId(),dto.getTaskId());
        kafkaProducer.sendMessage(Integer.toString(dto.getTagId()) + " " + Integer.toString(dto.getTaskId()), REMOVE_TOPIC);
    }

    @PostMapping("/newTag")
    public void newTag(@RequestBody TagDto dto) {
       tagService.storeNewTag(dto);
        //kafkaProducer.sendMessage(dto.getName(), NEW_TOPIC);
    }

    @Cacheable("tags")
    @GetMapping("/getTag")
    public String getTag(@RequestParam int id) throws InterruptedException {
        Thread.sleep(5000);
        return tagService.getTag(id);
    }

    @PostMapping("/deleteTag")
    public void deleteTag(@RequestParam int id){
        tagService.deleteTag(id);
    }

    @PostMapping("/updateTag")
    public void updateTag(@RequestBody TagDto tagDto){
        tagService.updateTag(tagDto);
    }

    @GetMapping("/tags")
    public List<TagDto> listAll(){
        List<Tag> tags = tagService.findAll();
        List<TagDto> dtos = new ArrayList<>();
        for(Tag t: tags){
            dtos.add(new TagDto(
                  t.getName(), t.getId()
                    )
            );
        }
        return dtos;
    }

}
