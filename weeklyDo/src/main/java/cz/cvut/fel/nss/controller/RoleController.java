package cz.cvut.fel.nss.controller;


import cz.cvut.fel.nss.dto.RoleDto;
import cz.cvut.fel.nss.model.Role;
import cz.cvut.fel.nss.service.RoleService;
import cz.cvut.fel.nss.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/user")
public class RoleController {
    @Autowired
    private RoleService roleService;

    @PostMapping("/updateRole")
    public void updateRole(@RequestBody RoleDto roleDto){
        roleService.updateRole(roleDto);
    }

    @GetMapping("/getRole")
    public RoleDto getRole(@RequestParam int id){
        return roleService.getRole(id);
    }

    @PostMapping("/createRole")
    public void createRole(@RequestBody RoleDto roleDto){
        roleService.createRole(roleDto);
    }

    @PostMapping("/deleteRole")
    public void deleteRole(@RequestParam int id){
        roleService.deleteRole(id);
    }

    @GetMapping("/roles")
    public List<RoleDto> listAll(){
        List<Role> roles = roleService.findAll();
        List<RoleDto> dtos = new ArrayList<>();
        for(Role r:roles){
            dtos.add(new RoleDto(
                    String.valueOf(r.getId()),
                    r.getName()
            ));
        }
        return dtos;
    }
}
