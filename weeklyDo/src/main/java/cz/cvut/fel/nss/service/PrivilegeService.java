package cz.cvut.fel.nss.service;


import cz.cvut.fel.nss.dao.PrivilegeDao;
import cz.cvut.fel.nss.dao.RoleDao;
import cz.cvut.fel.nss.dao.UserDao;
import cz.cvut.fel.nss.dto.PrivilegeDto;
import cz.cvut.fel.nss.dto.RoleDto;
import cz.cvut.fel.nss.model.Privilege;
import cz.cvut.fel.nss.model.Role;
import cz.cvut.fel.nss.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PrivilegeService {
    @Autowired
    PrivilegeDao privilegeDao;

    @Autowired
    RoleDao roleDao;

    @Transactional
    public void deletePrivilege(int id){
        Privilege privilege = privilegeDao.getOne((long) id);
        for(Role role: roleDao.findAll()){
            role.getPrivileges().remove(privilege);
            roleDao.save(role);
        }
        privilegeDao.delete(privilege);

    }

    @Transactional
    public void updatePrivilege(PrivilegeDto privilegeDto){
        Privilege privilege = privilegeDao.getOne(Long.parseLong(privilegeDto.getId()));
        privilege.setName(privilegeDto.getName());
        privilegeDao.save(privilege);
    }

    @Transactional
    public void createPrivilege(PrivilegeDto privilegeDto){
        Privilege privilege = new Privilege();
        privilege.setName(privilegeDto.getName());
        privilegeDao.save(privilege);
    }

    @Transactional
    public PrivilegeDto getPrivilege(int id){
        PrivilegeDto privilegeDto = new PrivilegeDto();
        Privilege privilege = privilegeDao.getOne((long) id);
        privilegeDto.setName(privilege.getName());
        privilegeDto.setId(String.valueOf(privilege.getId()));
        return privilegeDto;
    }


    public List<Privilege> findAll() {
        return privilegeDao.findAll();
    }
}
