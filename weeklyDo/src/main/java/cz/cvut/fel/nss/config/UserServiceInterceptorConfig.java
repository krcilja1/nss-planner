package cz.cvut.fel.nss.config;

import cz.cvut.fel.nss.Interceptors.UserServiceInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Component
public class UserServiceInterceptorConfig implements WebMvcConfigurer {
    @Autowired
    UserServiceInterceptor userServiceInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(userServiceInterceptor);
    }

}
