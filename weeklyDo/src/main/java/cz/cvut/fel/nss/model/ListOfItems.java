package cz.cvut.fel.nss.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;

@Entity
public class ListOfItems extends AbstractEntity {
    @Getter
    @Setter
    private String name;

    @OneToMany(cascade = CascadeType.PERSIST, orphanRemoval = true)
    @JoinTable(name = "List_items")
    @LazyCollection(LazyCollectionOption.FALSE)
    private java.util.List<ListItem> listItems;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @LazyCollection(LazyCollectionOption.FALSE)
    private User user;

}
