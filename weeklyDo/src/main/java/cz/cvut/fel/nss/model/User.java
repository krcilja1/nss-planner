package cz.cvut.fel.nss.model;


import cz.cvut.fel.nss.dto.UserDto;
import cz.cvut.fel.nss.security.ValidEmail;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Entity
@Table(name = "NSS_user")
@NamedQueries({
        @NamedQuery(name = "User.findByUsername", query ="SELECT u FROM User u WHERE username = ?1"),
        @NamedQuery(name = "User.findByEmail", query ="SELECT u FROM User u WHERE email = ?1")
})
public class User extends AbstractEntity {

    private String firstName;
    @Transient
    private String passwordConfirm;
    private String lastName;
    private String username;
    private String password;

    @ValidEmail
    @NotNull
    @NotEmpty
    private String email;
    private int phone;

    @Enumerated(EnumType.STRING)
    private BusyLevel busyLevel;

    @Getter
    @Setter
    private int experienceRating = 0;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"))
    private Collection<Role> roles;

    @ManyToMany
    @JoinTable(
            name = "user_tags",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "tag_id", referencedColumnName = "id"))
    private Collection<Tag> tags;

    @OneToMany(cascade = CascadeType.PERSIST, orphanRemoval = true)
    @JoinTable(name = "user_notifications")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Notification> notifications;

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    public Collection<Tag> getTags() {
        return tags;
    }

    public void setTags(Collection<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public String toString() {
        return "User{" +
                firstName + " " + lastName +
                "(" + username + ")}";
    }

    public UserDto convertToDto(){
        UserDto result = new UserDto();
        result.setFirstName(this.firstName);
        result.setLastName(this.lastName);
        result.setEmail(this.email);
        result.setUsername(this.username);
        result.setPhone(Integer.toString(this.phone));
        return result;
    }

    @ManyToMany()
    @JoinTable(name = "team_user",
          joinColumns = @JoinColumn(
                  name = "user_id", referencedColumnName = "id"),
          inverseJoinColumns = @JoinColumn(
                    name = "team_id", referencedColumnName = "id"))
    @LazyCollection(LazyCollectionOption.FALSE)
    private Collection<Team> team;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "user_task",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "task_id", referencedColumnName = "id"))
    @LazyCollection(LazyCollectionOption.FALSE)
    private Set<Task> subscribedTasks;


    @OneToMany(cascade = CascadeType.PERSIST, orphanRemoval = true)
    @JoinTable(name = "user_lists")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ListOfItems> userLists;



    @OneToMany(cascade = CascadeType.PERSIST, orphanRemoval = true)
    @JoinTable(name = "user_task_owned")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Task> ownedTasks;



    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

//    public Collection<Task> getParticipatingTasks() {
//        return participatingTasks;
//    }
//
//    public void setParticipatingTasks(Collection<Task> participatingTasks) {
//        this.participatingTasks = participatingTasks;
//    }

    public BusyLevel getBusyLevel() {
        return busyLevel;
    }

    public void setBusyLevel(BusyLevel busyLevel) {
        this.busyLevel = busyLevel;
    }

    public List<Task> getOwnedTasks() {
        return ownedTasks;
    }

    public void setOwnedTasks(List<Task> ownedTasks) {
        this.ownedTasks = ownedTasks;
    }

    public void setTeam(Collection<Team> team) {
        this.team = team;
    }

    public Set<Task> getSubscribedTasks() {
        return subscribedTasks;
    }

    public void setSubscribedTasks(Set<Task> userTasks) {
        this.subscribedTasks = userTasks;
    }

    public List<ListOfItems> getUserLists() {
        return userLists;
    }

    public void setUserLists(List<ListOfItems> userLists) {
        this.userLists = userLists;
    }

    public String getFirstName() {
        return firstName;
    }
    public String getPasswordConfirm() {
        return passwordConfirm;
    }
    public String getLastName() {
        return lastName;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public int getPhone() {
        return phone;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }
    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Collection<Team> getTeam() {
        return team;
    }

    public void setTeam(List<Team> team) {
        this.team = team;
    }
}
