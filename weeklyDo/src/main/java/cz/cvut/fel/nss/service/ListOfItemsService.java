package cz.cvut.fel.nss.service;

import cz.cvut.fel.nss.dao.ListOfItemsDao;
import cz.cvut.fel.nss.dao.UserDao;
import cz.cvut.fel.nss.dto.ListOfItemsDto;
import cz.cvut.fel.nss.model.ListOfItems;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import java.util.List;

@Service
public class ListOfItemsService {
    private final UserDao userDao;
    private final ListOfItemsDao listOfItemsDao;

    @Autowired
    public ListOfItemsService(ListOfItemsDao listOfItemsDao, UserDao userDao) {
        this.listOfItemsDao = listOfItemsDao;
        this.userDao = userDao;
    }

    @Transactional
    public List<ListOfItems> findAll() {
        return listOfItemsDao.findAll();
    }

    @Transactional
    public void createListOfItems(ListOfItemsDto dto){
        ListOfItems listOfItems = new ListOfItems();
        listOfItems.setName(dto.getName());
        listOfItems.setUser(userDao.getOne(Integer.parseInt(dto.getUserId())));
        listOfItemsDao.save(listOfItems);
    }

    @Transactional
    public void updateListOfItems(ListOfItemsDto dto){
        ListOfItems listOfItems = listOfItemsDao.getOne(Integer.parseInt(dto.getId())) ;
        listOfItems.setName(dto.getName());
        listOfItems.setUser(userDao.getOne(Integer.parseInt(dto.getUserId())));
        listOfItemsDao.save(listOfItems);
    }

    @Transactional
    public void deleteListOfItems(int id){
        listOfItemsDao.delete(
                listOfItemsDao.getOne(id)
        );
    }

    @Transactional
    public ListOfItemsDto getListOfItems(int id){
        ListOfItems listOfItems = listOfItemsDao.getOne(id);
        ListOfItemsDto dto = new ListOfItemsDto();
        dto.setId(String.valueOf(listOfItems.getId()));
        dto.setName(listOfItems.getName());
        dto.setUserId(String.valueOf(listOfItems.getUser().getId()));
        return dto;
    }
}
