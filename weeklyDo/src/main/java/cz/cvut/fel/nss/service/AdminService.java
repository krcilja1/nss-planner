package cz.cvut.fel.nss.service;


import cz.cvut.fel.nss.UserProxy;
import cz.cvut.fel.nss.dao.RoleDao;
import cz.cvut.fel.nss.dao.UserDao;
import cz.cvut.fel.nss.model.Role;
import cz.cvut.fel.nss.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;


@Service
public class AdminService implements AdminServiceInterface{
    private final UserDao userDao;
    private final RoleDao roleDao;

    @Autowired
    public AdminService(UserDao userDao, RoleDao roleDao) {
        this.userDao = userDao;
        this.roleDao = roleDao;
    }

    @KafkaListener(topics="admin-approve")
    public void kafkaApproveManager(String message){
        approveManager(Integer.parseInt(message));
    }


    @Transactional
    public void approveManager(int id){
        UserProxy user = new UserProxy(userDao.getOne(id));
        List<Role> roles = (List<Role>) user.getRoles();
        roles.add(roleDao.findByName("ROLE_MANAGER").get(0));
        userDao.save(user.getUser());
    }
}
