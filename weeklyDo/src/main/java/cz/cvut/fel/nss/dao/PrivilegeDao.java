package cz.cvut.fel.nss.dao;

import cz.cvut.fel.nss.model.Privilege;
import cz.cvut.fel.nss.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface PrivilegeDao extends JpaRepository<Privilege, Long> {
    List<Privilege> findByName(String name);
}
