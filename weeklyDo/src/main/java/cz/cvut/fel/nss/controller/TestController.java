package cz.cvut.fel.nss.controller;

        import cz.cvut.fel.nss.service.ConsumerService;
        import cz.cvut.fel.nss.service.ProducerService;
        import cz.cvut.fel.nss.service.UserService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.PostMapping;
        import org.springframework.web.bind.annotation.RequestParam;
        import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    private final ProducerService producer;

    @Autowired
    public TestController(ProducerService producer, UserService consumer) {
        this.producer = producer;
    }

    @PostMapping("/publish")
    public void messageToTopic(@RequestParam("message") String message){
        this.producer.sendMessage(message,"test_topic");
    }
}
