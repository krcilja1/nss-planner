package cz.cvut.fel.nss.service;

import cz.cvut.fel.nss.dao.TeamDao;
import cz.cvut.fel.nss.dao.UserDao;
import cz.cvut.fel.nss.dto.TaskDto;
import cz.cvut.fel.nss.dto.TeamDto;
import cz.cvut.fel.nss.dto.UserDto;
import cz.cvut.fel.nss.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import javax.transaction.Transactional;
import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.util.*;

@Service
public class TeamService {
    private TeamDao teamDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    public TeamService(TeamDao teamDao) {
        this.teamDao = teamDao;
    }

    @Autowired
    public UserService userService;



    @Transactional
    public int createTeam(TeamDto teamDto){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Team team = new Team();
        team.setName(teamDto.getName());
        User user = userDao.findByUsername(username).get(0);
        team.setUserList(Collections.singletonList(user));
        user.getTeam().add(team);
        teamDao.save(team);
        userDao.save(user);
        return team.getId();
    }

    @Transactional
    public List<Task> getLastWeeksTeamTasks(int id) throws Exception {
        long DAY_IN_MS = 1000 * 60 * 60 * 24;
        Date lastWeek = new Date(System.currentTimeMillis() - (7 * DAY_IN_MS));


        List<Team> teams = teamDao.findById(id);
        if(teams==null) throw new Exception("No such team");
        Team team = teams.get(0);
        List<Task> tasks = new ArrayList<>();
        for(User u: team.getUserList()){
            for(Task t: u.getSubscribedTasks()){

                if(!tasks.contains(t) && t.getCreatedAt().after(lastWeek)){
                    tasks.add(t);
                }
            }
        }
        return tasks;
    }

    @Transactional
    public TeamDto getTeam(int id){
        TeamDto team = new TeamDto();
        Team t = teamDao.getOne(id);
        team.setId(Integer.toString(t.getId()));
        team.setName(t.getName());
        return team;
    }

    @Transactional
    public void deleteTeam(int id){
        Team team = teamDao.getOne(id);
        for(User u: userDao.findAll()){
            u.getTeam().remove(team);
            userDao.save(u);
        }

        teamDao.delete(team);
    }

    @Transactional
    public List<Team> findAll(){
        return teamDao.findAll();
    }

    public void updateTeam(TeamDto dto) {
        Team team = teamDao.getOne(Integer.parseInt(dto.getId()));
        team.setName(dto.getName());
        teamDao.save(team);
    }

    @Transactional
    public void changeTeamState(int teamId, BusyLevel busyLevel){
        Team team = teamDao.getOne(teamId);
        for (User u : team.getUserList()){
            u.setBusyLevel(busyLevel);
            userService.notify(u, new Notification(new java.util.Date(System.currentTimeMillis()), "Your state has been changed to " + busyLevel.getName() + " by the manager"));
        }
        teamDao.save(team);
    }
}
