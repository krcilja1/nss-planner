package cz.cvut.fel.nss.controller;

import cz.cvut.fel.nss.dao.TaskDao;
import cz.cvut.fel.nss.dto.TaskDto;
import cz.cvut.fel.nss.dto.UserDto;
import cz.cvut.fel.nss.model.Task;
import cz.cvut.fel.nss.model.User;
import cz.cvut.fel.nss.service.TaskService;
import cz.cvut.fel.nss.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.CurrentSecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/user")
public class TaskController {

    @Autowired
    private UserService userService;

    @Autowired
    private TaskService taskService;


    @GetMapping("/tasks")
    public List<TaskDto> listAll() {
        List<Task> tasks = taskService.findAll();
        List<TaskDto> result = new ArrayList<>();
        for(Task t : tasks){
            result.add(t.convertToDto());
        }
        return result;
    }



//    @PostMapping("/deleteTask")
//    public void deleteTask(@RequestParam int id){
//        taskService.deleteTask(id);
//    }

    @PostMapping("/deleteTask")
    public void deleteTask(@RequestBody TaskDto taskDto){
        taskService.deleteTask(taskDto.getId());
    }

    @GetMapping("/getTask")
    public TaskDto getTask(@RequestParam int id){
        return taskService.getTask(id);
    }

    @PostMapping("/updateTask")
    public void updateTask(@RequestBody TaskDto taskDto) throws ParseException {
        taskService.updateTask(taskDto);

    }

    @PostMapping("/closeTask")
    public String closeTask(@RequestBody TaskDto taskDto){
        return taskService.closeTask(taskDto.getId());
    }
}