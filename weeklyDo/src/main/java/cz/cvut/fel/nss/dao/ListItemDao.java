package cz.cvut.fel.nss.dao;

import cz.cvut.fel.nss.model.ListItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ListItemDao extends JpaRepository<ListItem, Integer > {
}
