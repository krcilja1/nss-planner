package cz.cvut.fel.nss.service;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class ConsumerService {
    @KafkaListener(id = "myId",topics="test_topic")
    public void consumeMessage(String message){
        System.out.println(message);
    }
}
