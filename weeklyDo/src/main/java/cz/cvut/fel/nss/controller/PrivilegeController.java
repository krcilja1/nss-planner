package cz.cvut.fel.nss.controller;

import cz.cvut.fel.nss.dto.PrivilegeDto;
import cz.cvut.fel.nss.dto.RoleDto;
import cz.cvut.fel.nss.model.Privilege;
import cz.cvut.fel.nss.service.PrivilegeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/user")
public class PrivilegeController {
    @Autowired
    PrivilegeService privilegeService;


    @GetMapping("/getPrivilege")
    public PrivilegeDto getPrivilege(@RequestParam int id){
        return privilegeService.getPrivilege(id);
    }

    @PostMapping("/createPrivilege")
    public void createPrivilege(@RequestBody PrivilegeDto privilegeDto){
        privilegeService.createPrivilege(privilegeDto);
    }

    @PostMapping("/updatePrivilege")
    public void updatePrivilege(@RequestBody PrivilegeDto privilegeDto){
        privilegeService.updatePrivilege(privilegeDto);
    }
    @PostMapping("/deletePrivilege")
    public void deletePrivilege(@RequestParam int id){
        privilegeService.deletePrivilege(id);
    }

    @GetMapping("/privileges")
    public List<PrivilegeDto> listAll(){
        List<Privilege> privileges = privilegeService.findAll();
        List<PrivilegeDto> dtos = new ArrayList<>();
        for(Privilege p: privileges){
            dtos.add(new PrivilegeDto(
                    String.valueOf(p.getId()),
                    p.getName()
            ));
        }
        return dtos;
    }
}
