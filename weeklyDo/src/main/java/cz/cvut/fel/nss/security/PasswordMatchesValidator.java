package cz.cvut.fel.nss.security;

import cz.cvut.fel.nss.UserProxy;
import cz.cvut.fel.nss.model.User;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator
        implements ConstraintValidator<PasswordMatches, Object> {

    @Override
    public void initialize(PasswordMatches constraintAnnotation) {
    }
    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context){
        UserProxy user = new UserProxy((User) obj);
        return user.getPassword().equals(user.getPasswordConfirm());
    }
}