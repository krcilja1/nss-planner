package cz.cvut.fel.nss.service;

import cz.cvut.fel.nss.dao.TagDao;
import cz.cvut.fel.nss.dao.TaskDao;
import cz.cvut.fel.nss.dto.TagDto;
import cz.cvut.fel.nss.model.Tag;
import cz.cvut.fel.nss.model.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TagService {

    @Autowired
    private TaskDao taskDao;

    @Autowired
    private TagDao tagDao;

    @Autowired
    private TaskService taskService;

    @Transactional
    public Tag findByName(String name) throws Exception {
        List<Tag> temp = tagDao.findByName(name);
        if(temp.isEmpty()) throw new Exception("No tag with name of " + name);
        return temp.get(0);
    }

    @Transactional
    public List<Tag> findByTask(int taskId){
        List<Task> tasks = taskDao.findAll();
        List<Tag> result = new ArrayList<>();
        for(Task task : tasks){
            if(task.getId()==taskId){
                return task.getTags().parallelStream().collect(Collectors.toUnmodifiableList());
            }
        }
        return new ArrayList<>();
    }

    @KafkaListener(topics="add-tag-task")
    public void kafkaAddTask(String message) {
        String [] ids = message.split("\\s+");
        addTask(Integer.parseInt(ids[0]), Integer.parseInt(ids[1]));
    }

    @KafkaListener(topics="remove-tag-task")
    public void kafkaRemoveTask(String message) {
        String [] ids = message.split("\\s+");
        removeTask(Integer.parseInt(ids[0]), Integer.parseInt(ids[1]));
    }
    @KafkaListener(topics="new-tag-task")
    public void kafkaNewTask(String message) {
        TagDto dto = new TagDto(message);
        storeNewTag(dto);
    }

    @Transactional
    public void addTask(int tagID, int taskId){
        List<Tag> tags = tagDao.findById(tagID);
        List<Task> tasks = taskDao.findById(taskId);
        if(tags!=null && tasks!=null){
            Tag tag = tags.get(0);
            Task task = tasks.get(0);
            tag.getTasks().add(task);
            task.getTags().add(tag);
            taskDao.save(task);
            tagDao.save(tag);
        }

    }

    @Transactional
    public void removeTask(int tagID, int taskId){
        List<Tag> tags = tagDao.findById(tagID);
        List<Task> tasks = taskDao.findById(taskId);
        if(tags!=null && tasks!=null){
            Tag tag = tags.get(0);
            Task task = tasks.get(0);
            tag.getTasks().remove(task);
            task.getTags().remove(tag);
            taskDao.save(task);
            tagDao.save(tag);
        }
    }

    @Transactional
    public void storeNewTag(TagDto dto){
        Tag tag = new Tag();
        tag.setName(dto.getName());
        List<Task> tasks = new ArrayList<>();
        tag.setTasks(tasks);
        tagDao.save(tag);
    }

    @Transactional
    public void deleteTag(int id){
        Tag tag = tagDao.getOne(id);
        for(Task task: taskDao.findAll()){
            task.getTags().remove(tag);
            taskDao.save(task);
        }
        tagDao.delete(tag);
    }

    @Transactional
    public String getTag(int id){
        Tag tag = tagDao.getOne(id);
        TagDto dto = new TagDto();
        dto.setName(tag.getName());
        return dto.getName();
    }
    @Transactional
    public void updateTag(TagDto tagDto){
        Tag tag = tagDao.findByName(tagDto.getName()).get(0);
        tag.setName(tagDto.getName());
        tagDao.save(tag);
    }

    public List<Tag> findAll() {
        return tagDao.findAll();
    }
}
