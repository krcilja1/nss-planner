package cz.cvut.fel.nss.service;

import cz.cvut.fel.nss.dao.PrivilegeDao;
import cz.cvut.fel.nss.dao.RoleDao;
import cz.cvut.fel.nss.dao.UserDao;
import cz.cvut.fel.nss.dto.RoleDto;
import cz.cvut.fel.nss.model.Privilege;
import cz.cvut.fel.nss.model.Role;
import cz.cvut.fel.nss.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class RoleService {

    @Autowired
    RoleDao roleDao;

    @Autowired
    PrivilegeDao privilegeDao;

    @Autowired
    UserDao userDao;

    @Transactional
    public void deleteRole(int id){
        Role role = roleDao.getOne((long) id);
        for(User u: userDao.findAll()){
            u.getRoles().remove(role);
            userDao.save(u);
        }
        roleDao.delete(role);

    }

    @Transactional
    public void createRole(RoleDto roleDto){
        Role role = new Role();
        role.setName(roleDto.getName());
        roleDao.save(role);
    }

    @Transactional
    public void updateRole(RoleDto roleDto){
        Role role =roleDao.getOne(Long.parseLong(roleDto.getId()));
        role.setName(roleDto.getName());
        roleDao.save(role);
    }

    @Transactional
    public RoleDto getRole(int id){
        RoleDto dto = new RoleDto();
        Role role = roleDao.getOne((long) id);
        dto.setName(role.getName());
        dto.setId(Long.toString(role.getId()));
        return dto;
    }

    public List<Role> findAll() {
        return roleDao.findAll();
    }
}
