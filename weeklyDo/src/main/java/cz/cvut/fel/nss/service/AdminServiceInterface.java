package cz.cvut.fel.nss.service;

import cz.cvut.fel.nss.dao.RoleDao;
import cz.cvut.fel.nss.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;

public interface AdminServiceInterface {
    void approveManager(int id);
}
