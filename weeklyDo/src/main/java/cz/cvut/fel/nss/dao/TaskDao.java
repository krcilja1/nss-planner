package cz.cvut.fel.nss.dao;

import cz.cvut.fel.nss.model.Tag;
import cz.cvut.fel.nss.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskDao extends JpaRepository<Task, Integer> {
    List<Task> findById(int id);
    List<Task> findByName(String name);
}
