package cz.cvut.fel.nss.service;


import cz.cvut.fel.nss.UserProxy;
import cz.cvut.fel.nss.dao.RoleDao;
import cz.cvut.fel.nss.dao.UserDao;
import cz.cvut.fel.nss.model.Role;
import cz.cvut.fel.nss.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class ManagerService {
    private final UserDao userDao;
    private final RoleDao roleDao;

    @Autowired
    public ManagerService(UserDao userDao, RoleDao roleDao) {
        this.userDao = userDao;
        this.roleDao = roleDao;
    }

    @KafkaListener(topics="manager-approve")
    public void kafkaApproveUser(String message) {
        int id = Integer.parseInt(message);
        approveUser(id);
    }

    @Transactional
    public void approveUser(int id){
        UserProxy user = new UserProxy(userDao.getOne(id));
        Set<Role> roles = new HashSet<>();
        roles.add(roleDao.findByName("ROLE_USER").get(0));
        user.setRoles(roles);
        userDao.save(user.getUser());
    }

    @Transactional
    public List<User> findAll() {
        List<User> ret = userDao.findAll();
        return ret;
    }
}
