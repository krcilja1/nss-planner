package cz.cvut.fel.nss.service;

import cz.cvut.fel.nss.dao.ListItemDao;
import cz.cvut.fel.nss.dao.ListOfItemsDao;
import cz.cvut.fel.nss.dto.ListItemDto;
import cz.cvut.fel.nss.model.ListItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.InvalidTimeoutException;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ListItemService {

    final ListItemDao listItemDao;
    final ListOfItemsDao listOfItemsDao;

    @Autowired
    public ListItemService(cz.cvut.fel.nss.dao.ListItemDao listItemDao, ListOfItemsDao listOfItemsDao) {
        this.listItemDao = listItemDao;
        this.listOfItemsDao = listOfItemsDao;
    }

    @Transactional
    public List<ListItem> findAll(){
        return listItemDao.findAll();
    }

    @Transactional
    public void createListItem(ListItemDto dto){
        ListItem item = new ListItem();
        item.setContent(dto.getContent());
        item.setList(
                listOfItemsDao.getOne(
                        Integer.parseInt(
                                dto.getListOfItemId()
                        )
                )
        );
        listItemDao.save(item);
    }

    @Transactional
    public void updateListItem(ListItemDto dto){
        ListItem item = listItemDao.getOne(Integer.parseInt(dto.getId()));
        item.setContent(dto.getContent());
        item.setList(
                listOfItemsDao.getOne(
                        Integer.parseInt(
                                dto.getListOfItemId()
                        )
                )
        );
        listItemDao.save(item);
    }

    @Transactional
    public ListItemDto getListItem(int id){
        ListItemDto dto = new ListItemDto();
        ListItem item = listItemDao.getOne(id);
        dto.setId(String.valueOf(item.getId()));
        dto.setListOfItemId(String.valueOf(item.getList().getId()));
        dto.setContent(item.getContent());
        return dto;
    }

    @Transactional
    public void deleteListItem(int id){
        listItemDao.delete(
                listItemDao.getOne(id)
        );
    }
}
