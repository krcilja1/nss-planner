package cz.cvut.fel.nss.model;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Entity
@NamedQueries({
        @NamedQuery(name = "Tag.findByName", query ="SELECT t FROM Tag t WHERE name = ?1"),
        @NamedQuery(name = "Tag.findById", query ="SELECT t FROM Tag t WHERE id = ?1")
})
public class Tag extends AbstractEntity {
    @ManyToMany(fetch = FetchType.EAGER,mappedBy = "tags")
    private List<Task> tasks;

    @ManyToMany(mappedBy = "tags")
    private Set<User> users;

    private String name;

    public Collection<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
