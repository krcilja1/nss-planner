package cz.cvut.fel.nss.decorators;

import cz.cvut.fel.nss.service.AdminService;
import cz.cvut.fel.nss.service.AdminServiceInterface;
import org.apache.kafka.clients.admin.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class AdminServiceDecorator implements AdminServiceInterface {


    AdminService service;

    @Autowired
    public AdminServiceDecorator(AdminService service) {
        this.service = service;
    }

    @Transactional
    @Override
    public void approveManager(int id) {
        this.service.approveManager(id);
        System.out.println("Manager Approval Complete");
    }
}
