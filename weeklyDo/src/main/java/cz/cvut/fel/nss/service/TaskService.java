package cz.cvut.fel.nss.service;

import cz.cvut.fel.nss.UserProxy;
import cz.cvut.fel.nss.dao.TagDao;
import cz.cvut.fel.nss.dao.TaskDao;
import cz.cvut.fel.nss.dao.UserDao;
import cz.cvut.fel.nss.dto.TagDto;
import cz.cvut.fel.nss.dto.TaskDto;
import cz.cvut.fel.nss.model.Tag;
import cz.cvut.fel.nss.model.Task;
import cz.cvut.fel.nss.model.User;
import org.hibernate.exception.DataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TaskService {
    @Autowired
    private TaskDao taskDao;

    @Autowired
    private TagDao tagDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserService userService;
    @Transactional
    public void save(Task t){
        taskDao.save(t);
    }

    @Transactional
    public List<Task> findByTag(int tagId) throws Exception {
        List<Tag> tags = tagDao.findAll();
        List<Task> result = new ArrayList<>();
        for(Tag tag : tags){
            if(tag.getId()==tagId){
                return tag.getTasks().parallelStream().collect(Collectors.toUnmodifiableList());
            }
        }
        return new ArrayList<>();
    }

    @Transactional
    public Task findByName(String name) {
        List<Task> temp = taskDao.findByName(name);
        if(temp.isEmpty()) {
            System.out.println("No task with name of " + name);
            return null;
        }
        return temp.get(0);
    }

    @Transactional
    public void addTag(Tag t, Task task){
        task.getTags().add(t);
        taskDao.save(task);
    }

    @Transactional
    public void removeTag(Tag t, Task task){
        task.getTags().remove(t);
        taskDao.save(task);
    }

    @Transactional
    public void createTask(TaskDto taskDto) throws ParseException {
        Task task = new Task();
        task.setClosed(taskDto.isClosed());
        task.setContent(taskDto.getContent());
        task.setName(taskDto.getName());
        task.setDeadline(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.sss").parse(taskDto.getDeadline()));
        taskDao.save(task);
    }

    @Transactional
    public void updateTask(TaskDto taskDto) throws ParseException {
        Task task = taskDao.getOne(taskDto.getId());
        task.setClosed(taskDto.isClosed());
        task.setContent(taskDto.getContent());
        task.setName(taskDto.getName());
        task.setDeadline(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.sss").parse(taskDto.getDeadline()));
        taskDao.save(task);
    }

    @Transactional
    public TaskDto getTask(int id){
        Task task = taskDao.getOne(id);
        return new TaskDto(id,task.getContent(),task.getDeadline().toString(),task.getName(),task.isClosed());
    }

    @Transactional
    public void deleteTask(int task){
        Task t = taskDao.getOne(task);
        User u = t.getOwner();
        u.getOwnedTasks().remove(t);
        u.getSubscribedTasks().remove(t);
        t.setOwner(null);
        userDao.save(u);
        taskDao.save(t);
        for(Tag tag : tagDao.findAll()){
            tag.getTasks().remove(t);
            tagDao.save(tag);
        }
        for(User user: userDao.findAll()){

            user.getSubscribedTasks().remove(t);
            user.getOwnedTasks().remove(t);
            userDao.save(user);
        }
        taskDao.delete(t);
    }

    @Transactional
    public List<Task> findAll(){
        return taskDao.findAll();
    }

    @Transactional
    public String closeTask(int taskId){
        Task task = taskDao.getOne(taskId);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        UserProxy user = new UserProxy(userDao.findByUsername(username).get(0));
        if(user.getUser().getId().equals(task.getOwner().getId())){
            if(!task.isClosed()){
                task.setClosed(true);
                List<User> temp = new ArrayList<User>(task.getParticipants());
                for(User u: temp){
                    if(u.getId().equals(task.getOwner().getId())){
                        task.setOwner(null);
                    }
                    u.setExperienceRating(u.getExperienceRating()+1);
                    u.getSubscribedTasks().remove(user.getSubscribedTasks().stream().filter(t -> t.getName().equals(task.getName())).findFirst().orElse(null));
                    userDao.save(u);
                }
                return "Task was closed";
            }
            return "Task is already closed";
        }
        return "You are not the task owner, task wasn't closed";
    }
}
