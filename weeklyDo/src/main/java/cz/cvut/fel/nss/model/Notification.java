package cz.cvut.fel.nss.model;


import org.aspectj.weaver.ast.Not;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
public class Notification extends AbstractEntity{
    private Date dateCreated;
    private String name;

    public Notification(Date dateCreated, String name) {
        this.dateCreated = dateCreated;
        this.name = name;
    }

    public Notification() {
    }


    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
