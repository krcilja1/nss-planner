package cz.cvut.fel.nss.dto;

public class TagTaskIdDto {
    int tagId;
    int taskId;

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public TagTaskIdDto(int tagId, int taskId) {
        this.tagId = tagId;
        this.taskId = taskId;
    }

    public TagTaskIdDto() {
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }
}
