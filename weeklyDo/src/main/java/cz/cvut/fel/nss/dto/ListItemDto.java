package cz.cvut.fel.nss.dto;

public class ListItemDto {
    String id, content, listOfItemId;

    public ListItemDto(String content, String listOfItemId) {
        this.content = content;
        this.listOfItemId = listOfItemId;
    }

    public ListItemDto() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getListOfItemId() {
        return listOfItemId;
    }

    public void setListOfItemId(String listOfItemId) {
        this.listOfItemId = listOfItemId;
    }
}
