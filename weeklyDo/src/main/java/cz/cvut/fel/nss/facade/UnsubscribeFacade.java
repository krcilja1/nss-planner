package cz.cvut.fel.nss.facade;

import cz.cvut.fel.nss.dto.TaskDto;

public interface UnsubscribeFacade {
    public void Unsubscribe(String username, TaskDto task);
}
