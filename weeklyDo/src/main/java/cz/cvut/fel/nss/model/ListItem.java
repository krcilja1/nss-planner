package cz.cvut.fel.nss.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;

@Entity
public class ListItem extends AbstractEntity {
    @Getter
    @Setter
    private String content;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "list_id")
    @LazyCollection(LazyCollectionOption.FALSE)
    private ListOfItems list;


}

