package cz.cvut.fel.nss.dao;

import cz.cvut.fel.nss.model.Tag;
import cz.cvut.fel.nss.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TagDao extends JpaRepository<Tag, Integer> {
    List<Tag> findByName(String name);
    List<Tag> findById(int id);

}