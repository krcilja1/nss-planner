package cz.cvut.fel.nss.controller;

import cz.cvut.fel.nss.dao.TeamDao;
import cz.cvut.fel.nss.dto.BusyLevelDto;
import cz.cvut.fel.nss.dto.TeamDto;
import cz.cvut.fel.nss.dto.UserDto;
import cz.cvut.fel.nss.model.*;
import cz.cvut.fel.nss.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/user")
public class TeamController {
    @Autowired
    private TeamService teamService;

    @GetMapping("/teams")
    public List<TeamDto> listAll() {
        List<Team> teams = teamService.findAll();
        List<TeamDto> result = new ArrayList<>();
        for (Team team :
                teams) {
            result.add(new TeamDto(
                    String.valueOf(team.getId()),
                    team.getName()
            ));
        }
        return result;
    }

    @PostMapping("/createTeam")
    public void createTeam(@RequestBody TeamDto dto){
         teamService.createTeam(dto);
    }

    @PostMapping("/updateTeam")
    public void updateTeam(@RequestBody TeamDto dto){
        teamService.updateTeam(dto);
    }
    @GetMapping("/teamWeekTasks")
    public String getLastWeeksTeamTasks(@RequestParam int id) throws Exception {
        List<Task> tasks = teamService.getLastWeeksTeamTasks(id);
        StringBuilder sb = new StringBuilder();
        for(Task task: tasks){
            sb.append("\n------------<Task Start>----------\n");
            sb.append("Task Name: ").append(task.getName()).append("\n");
            sb.append("Created at: ").append(task.getCreatedAt()).append(", Deadline: ").append(task.getDeadline()).append("\n");
            sb.append("Participants: ");
            for(User u: task.getParticipants()){
                sb.append(" ").append(u.getUsername());
            }
            sb.append("\nOwner: ").append(task.getOwner().getUsername());
            sb.append("\nClosed: ").append(task.isClosed());
            sb.append("\nTags: ");
            for (Tag tag: task.getTags()){
                sb.append(" ").append(tag);
            }
            sb.append("\n------------<Task End>----------\n");
        }
        return sb.toString();
    }

    @PostMapping("/changeTeamState")
    public void changeTeamState(@RequestParam int teamId, @RequestBody UserDto userDto){
        if(userDto.getBusyLevel().equals("free")){
            teamService.changeTeamState(teamId, BusyLevel.FREE);
        }
    }

    @GetMapping("/getTeam")
    public TeamDto getTeam(@RequestParam int id){
        return teamService.getTeam(id);
    }

    @PostMapping("/deleteTeam")
    public void deleteTeam(@RequestParam int id){
        teamService.deleteTeam(id);
    }


    

}