package cz.cvut.fel.nss.service;

import cz.cvut.fel.nss.UserProxy;
import cz.cvut.fel.nss.dao.*;
import cz.cvut.fel.nss.dto.TagListDto;
import cz.cvut.fel.nss.dto.TaskDto;
import cz.cvut.fel.nss.dto.TaskTagDto;
import cz.cvut.fel.nss.dto.UserDto;
import cz.cvut.fel.nss.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.CurrentSecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLOutput;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private TaskDao taskDao;

    @Autowired
    private TeamDao teamDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private final ProducerService producer;

    @Autowired
    private TagDao tagDao;

    @Autowired
    private NotificationDao notificationDao;

    @Autowired
    private TaskService taskService;

    @Autowired
    public UserService(ProducerService producer) {
        this.producer = producer;
    }



    @Transactional
    public void save(User user) {
      userDao.save(user);
   }

    @Transactional
    public UserProxy findByUsername(String username) {
        List<User> temp = userDao.findByUsername(username);
        if(temp.isEmpty()) throw new org.springframework.security.core.userdetails.UsernameNotFoundException(username);
        System.out.println("Username = " + temp.get(0).getUsername() + "Password = " + temp.get(0).getPassword());
        return new UserProxy(temp.get(0));
    }


    @Transactional
    public void registerNewUserAccount(UserDto userDto){
        UserProxy user = new UserProxy(new User());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setEmail(userDto.getEmail());
        user.setUsername(userDto.getUsername());
        Set<Role> roles = new HashSet<>();
        roles.add(roleDao.findByName("ROLE_UNAPPROVED").get(0));
        user.setRoles(roles);
        userDao.save(user.getUser());
        findAllManagersAndNotify(userDto.getUsername());
    }

    public void findAllManagersAndNotify(String username){
        List<User> managers = userDao.findAll().stream().filter(p -> p.getRoles().stream().anyMatch(r -> r.getName().equals("ROLE_MANAGER"))).collect(Collectors.toList());
        User user = userDao.findByUsername(username).get(0);
        for(User m : managers){
            notify(m, new Notification(new Date(System.currentTimeMillis()), "A user " + username + " with ID " + user.getId() + " is waiting for approval"));
        }
    }

    private boolean emailExists(String email) {
        return userDao.findByEmail(email) != null;
    }


    @Transactional
    public int createTask(TaskTagDto taskTagDto){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        Task temp = new Task();
        temp.setCreatedAtDate(new Date(System.currentTimeMillis()));
        temp.setClosed(taskTagDto.isClosed());
        temp.setContent(taskTagDto.getContent());
        temp.setName(taskTagDto.getName());
        temp.setDeadline(new Date(System.currentTimeMillis()));
        List<User> users = userDao.findByUsername(username);
        UserProxy user= new UserProxy(users.get(0));
        List<User> t2 = new ArrayList<>();
        t2.add(user.getUser());
        List<Tag> tagList = new ArrayList<>();
        for (Integer i : taskTagDto.getTagIds()){
            tagList.add(tagDao.getOne(i));
        }
        temp.setTags(tagList);
        analyzeTags(tagList);
        temp.setParticipants(t2);
        temp.setOwner(user.getUser());
        user.getOwnedTasks().add(temp);
        user.getSubscribedTasks().add(temp);
        taskDao.save(temp);
        userDao.save(user.getUser());
        return temp.getId();
    }

    @Transactional
    public void updateUser(UserDto user){
       // Authentication auth = SecurityContextHolder.getContext().getAuthentication();
     //   String username = auth.getName();
        User u = userDao.findByUsername(user.getUsername()).get(0);
        u.setUsername(user.getUsername());
        u.setPassword(user.getPassword());
        u.setPhone(Integer.parseInt(user.getPhone()));
        u.setFirstName(user.getFirstName());
        u.setLastName(user.getLastName());
        u.setEmail(user.getEmail());
        userDao.save(u);
    }
    @Transactional
    public UserDto getUser(int id){
        UserProxy proxy = new UserProxy(userDao.getOne(id));
        UserDto dto = new UserDto();
        dto.setEmail(proxy.getEmail());
        dto.setUsername(proxy.getUsername());
        dto.setPassword(proxy.getPassword());
        dto.setFirstName(proxy.getFirstName());
        dto.setLastName(proxy.getLastName());
        dto.setPhone(Integer.toString(proxy.getPhone()));
        return dto;
    }

    @Transactional
    public void addUserToTeam(UserProxy user, int id){
        Team team = teamDao.getOne(id);
        if(!user.getTeam().contains(team)){
            user.getTeam().add(team);
            userDao.save(user.getUser());
        }
    }

    @Transactional
    public void deleteUser(int id){
        userDao.delete(userDao.getOne(id));
    }

    @Transactional
    public void unsubscribeTask(String username, Task task) {
        User user = userDao.findByUsername(username).get(0);
        Task toDeleteSubscribed = user.getSubscribedTasks().stream().filter(t -> t.getName().equals(task.getName())).findFirst().orElse(null);
        Task toDeleteOwned = user.getOwnedTasks().stream().filter(t -> t.getName().equals(task.getName())).findFirst().orElse(null);
        user.getSubscribedTasks().remove(toDeleteSubscribed);
        user.getOwnedTasks().remove(toDeleteOwned);
        userDao.save(user);
    }

    @Transactional
    public void addTags(String username, TagListDto tagListDto){
        User user = userDao.findByUsername(username).get(0);
        List<Integer> tags = tagListDto.getTags();
        List<Tag> result = new ArrayList<>();
        for(Integer i : tags){
            result.add(tagDao.getOne(i));
        }
        user.getTags().addAll(result);
        userDao.save(user);
    }
    @Transactional
    public List<User> findAll(){
        return userDao.findAll();
    }

    @Transactional
    public Set<Task> getSubscribedTasks(String username){
        User user = userDao.findByUsername(username).get(0);
        Set<Task> result = new HashSet<>();
        Set<Task> tasks = user.getSubscribedTasks();
        for(Task ta : tasks){
            if(!ta.getOwner().getId().equals(user.getId())){
                result.add(ta);
            }
        }
        return result;
    }

    public List<Task> getOwnedTasks(String username){
        User user = userDao.findByUsername(username).get(0);
        return user.getOwnedTasks();
    }

    public List<Task> getAvailableTasks(String username) {
        User user = userDao.findByUsername(username).get(0);
        List<Task> allTasks = taskService.findAll();
        List<Task> result = new ArrayList<>();
        for(Task t : allTasks){
            if(t.getOwner() == null){
                System.out.println("Uz uzavrenej task");
            }
            else if(t.getParticipants().stream().anyMatch(p -> p.getId().equals(user.getId())) || (t.getOwner().getId().equals(user.getId()))){
                System.out.println("Je tam");
            } else {
                result.add(t);
            }
        }
        return result;
    }

    public void analyzeTags(List<Tag> tagsList){
        List<User> temp = findAll();
        for(User u : temp){
            for(int i = 0; i < tagsList.size(); i++){
                if(u.getTags().contains(tagsList.get(i))){
                    notify(u, new Notification(new Date(System.currentTimeMillis()), "A task with your tag," + tagsList.get(i).getName() + " was created"));
                }
            }
        }
    }

    public void notify(User user, Notification notification){
        user.getNotifications().add(notification);
        notificationDao.save(notification);
    }

    public List<Notification> getAllNotifications(String username){
        User user = userDao.findByUsername(username).get(0);
        return user.getNotifications();
    }


    public List<Long> getUsersRole(String username) {
        User user = userDao.findByUsername(username).get(0);
        List<Role> roles = (List<Role>) user.getRoles();
        List<Long> result = new ArrayList<>();
        for(Role r : roles){
            result.add(r.getId());
        }
        return result;
    }
}
