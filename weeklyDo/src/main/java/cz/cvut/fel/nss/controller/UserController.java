package cz.cvut.fel.nss.controller;

import cz.cvut.fel.nss.UserProxy;
import cz.cvut.fel.nss.dto.*;
import cz.cvut.fel.nss.facade.UnsubscribeFacadeImpl;
import cz.cvut.fel.nss.model.Notification;
import cz.cvut.fel.nss.model.Task;
import cz.cvut.fel.nss.model.Team;
import cz.cvut.fel.nss.model.User;
import cz.cvut.fel.nss.service.TaskService;
import cz.cvut.fel.nss.service.UserService;

import org.aspectj.weaver.ast.Not;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private UnsubscribeFacadeImpl facade;


//    @GetMapping("/registration")
//    public String showRegistrationForm() {
//        return "registration";
//    }


    @PostMapping("/registration")
    public void registerUserAccount(@RequestBody UserDto user){
        userService.registerNewUserAccount(user);
    }

    @PostMapping("/user/add-user")
    public void addUserToTeam(@RequestParam int teamId) {
        String username = ((UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        System.out.println(username);
        UserProxy user = new UserProxy(userService.findByUsername(username).getUser());
        userService.addUserToTeam(user, teamId);
    }

    @PostMapping("/user/unsubscribe-task")
    public void unsubscribeTask(@RequestBody TaskDto task){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        facade.Unsubscribe(username, task);
    }

    @GetMapping("user/subscribedTasks")
    public List<TaskDto> getSubscribedTasks() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        Set<Task> tasks = userService.getSubscribedTasks(username);
        List<TaskDto> result = new ArrayList<>();
        for(Task t : tasks){
            result.add(t.convertToDto());
        }
        return result;
    }

    @GetMapping("user/ownedTasks")
    public List<TaskDto> getOwnedTasks() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        List<Task> tasks = userService.getOwnedTasks(username);
        List<TaskDto> result = new ArrayList<>();
        for(Task t : tasks){
            result.add(t.convertToDto());
        }
        return result;
    }

    @GetMapping("user/availableTasks")
    public List<TaskDto> getAvailableTasks(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        List<Task> tasks = userService.getAvailableTasks(username);
        List<TaskDto> result = new ArrayList<>();
        for(Task t : tasks){
            result.add(t.convertToDto());
        }
        return result;
    }

    @PostMapping("/user/addTask")
    public String addTask(@RequestBody TaskTagDto taskTag){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        Task t = taskService.findByName(taskTag.getName());
        if(t != null){
            User user = userService.findByUsername(username).getUser();
            user.getSubscribedTasks().add(t);
            t.getParticipants().add(user);
            taskService.save(t);
            userService.save(user);
        }
        else{
            userService.createTask(taskTag);
        }
        return "ok";
    }

    @PostMapping("user/addTag")
    public void addTagsToUser(@RequestBody TagListDto tags){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        userService.addTags(username, tags);
    }

    @PostMapping("/updateUser")
    public void updateUser(@RequestBody UserDto user){
        userService.updateUser(user);
    }

    @GetMapping("/getUser")
    public UserDto getUser(@RequestParam int id){
        return userService.getUser(id);
    }

    @PostMapping("/deleteUser")
    public void deleteUser(@RequestParam int id){
        userService.deleteUser(id);
    }

    @GetMapping("user/notifications")
    public List<NotificationDto> getAllNotifications(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        List<Notification> notifications = userService.getAllNotifications(username);
        List<NotificationDto> result = new ArrayList<>();
        for (Notification n : notifications) {
            result.add(new NotificationDto(n.getDateCreated(), n.getName()));
        }
        return result;
    }

    @GetMapping("/role")
    public List<Long> getUsersRole(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        return userService.getUsersRole(username);
    }





}