package cz.cvut.fel.nss.dto;

public class IdDto {
    int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
