package cz.cvut.fel.nss.dto;

import java.util.ArrayList;
import java.util.List;

public class TaskTagDto {
    private String id;
    private String content;
    private String deadline;
    private String name;
    private boolean closed;
    private List<Integer> tagIds;

    public TaskTagDto() {
        tagIds = new ArrayList<>();
    }

    public TaskTagDto(int id, String content, String deadline, String name, boolean closed, List<Integer> tagIds) {
        this.id = Integer.toString(id);
        this.content = content;
        this.deadline = deadline;
        this.name = name;
        this.closed = closed;
        this.tagIds = tagIds;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public List<Integer> getTagIds() {
        return tagIds;
    }

    public void setTaskId(List<Integer> tagIds) {
        this.tagIds = tagIds;
    }
}
