package cz.cvut.fel.nss.controller;

import cz.cvut.fel.nss.dao.ListItemDao;
import cz.cvut.fel.nss.dto.ListItemDto;
import cz.cvut.fel.nss.model.ListItem;
import cz.cvut.fel.nss.service.ListItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
public class ListItemController {
    @Autowired
    private ListItemService listItemService;

    @GetMapping("/listItems")
    public List<ListItemDto> listAll() {
        List<ListItem> listItems = listItemService.findAll();
        List<ListItemDto> result = new ArrayList<>();
        for (ListItem item: listItems){
            result.add(listItemService.getListItem(item.getId()));
        }

        return result;
    }

    @GetMapping("/getListItem")
    public ListItemDto getListItem(@RequestParam int id){
        return listItemService.getListItem(id);
    }

    @PostMapping("/deleteListItem")
    public void deleteListItem(@RequestParam int id){
        listItemService.deleteListItem(id);
    }

    @PostMapping("/createListItem")
    public void createListItem(@RequestBody ListItemDto dto){
        listItemService.createListItem(dto);
    }

    @PostMapping("/updateListItem")
    public void updateListItem(@RequestBody ListItemDto dto){
        listItemService.updateListItem(dto);
    }


}