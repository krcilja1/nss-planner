package cz.cvut.fel.nss.dto;

import java.util.Date;

public class TaskDto {
    private int id;
    private String content;
    private String deadline;
    private String name;
    private boolean closed;

    public TaskDto() {
    }

    public TaskDto(int id, String content, String deadline, String name, boolean closed) {
        this.id = id;
        this.content = content;
        this.deadline = deadline;
        this.name = name;
        this.closed = closed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public String getDeadline() {
        return deadline;
    }

    public String getName() {
        return name;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }
}
