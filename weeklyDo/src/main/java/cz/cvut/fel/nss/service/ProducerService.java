package cz.cvut.fel.nss.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ProducerService {
    //private static final String TOPIC = "test_topic";

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;



    public void sendMessage(String message, String TOPIC){
        this.kafkaTemplate.send(TOPIC, message);
    }
}
