package cz.cvut.fel.nss.dao;

import cz.cvut.fel.nss.model.Task;
import cz.cvut.fel.nss.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeamDao extends JpaRepository<Team, Integer> {
    List<Team> findById(int id);
}
