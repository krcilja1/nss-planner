package cz.cvut.fel.nss.controller;

import cz.cvut.fel.nss.decorators.AdminServiceDecorator;
import cz.cvut.fel.nss.dto.UserDto;
import cz.cvut.fel.nss.service.AdminService;
import cz.cvut.fel.nss.service.AdminServiceInterface;
import cz.cvut.fel.nss.service.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/admin")
@RestController
public class AdminController {

    @Autowired
    private AdminService service;
    private AdminServiceInterface adminService;

    @Autowired
    private ProducerService kafkaProducer;
    private static final String TOPIC = "admin-approve";

    @Autowired
    public AdminController() {
        this.adminService = new AdminServiceDecorator(service);
    }

    @PostMapping("/approve")
    public void approveUser(@RequestParam int id){
        kafkaProducer.sendMessage(Integer.toString(id), TOPIC);
        //adminService.approveManager(id);

    }
}
