package cz.cvut.fel.nss.dao;


import cz.cvut.fel.nss.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDao extends JpaRepository<User, Integer> {
    List<User> findByUsername(String username);

    List<User> findByEmail(String email);
}
