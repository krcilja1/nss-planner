package cz.cvut.fel.nss.service;

import cz.cvut.fel.nss.UserProxy;
import cz.cvut.fel.nss.dao.RoleDao;
import cz.cvut.fel.nss.dao.UserDao;
import cz.cvut.fel.nss.model.Privilege;
import cz.cvut.fel.nss.model.Role;
import cz.cvut.fel.nss.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Service("userDetailsService")
@Transactional
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleDao roleDao;

    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {

        UserProxy user = userService.findByUsername(username);
        if (user == null) {
            return new org.springframework.security.core.userdetails.User(
                    " ", " ", true, true, true, true,
                    getAuthorities(Arrays.asList(
                            roleDao.findByName("ROLE_USER").get(0))));
        }

        return new org.springframework.security.core.userdetails.User(
                user.getUsername(), user.getPassword(), true, true, true,
                true, getAuthorities(user.getRoles()));
    }

    private Collection<? extends GrantedAuthority> getAuthorities(
            Collection<Role> roles) {

        return processedRoles((List<Role>) roles);
    }

    private List<GrantedAuthority> processedRoles(List<Role> roles){
        List<GrantedAuthority> result = new ArrayList<>();
        for(Role r : roles){
            result.add(new SimpleGrantedAuthority(r.getName()));
        }
        return result;
    }
}