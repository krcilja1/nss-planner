package cz.cvut.fel.nss.Interceptors;


import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

@Component
public class UserServiceInterceptor implements HandlerInterceptor {
    private Logger log = Logger.getLogger("UserService");
    private FileHandler handler;

    public UserServiceInterceptor() {
        System.out.println(System.getProperty("user.dir"));
        try {
            handler = new FileHandler(System.getProperty("user.dir") + "\\UserService.log");
            log.addHandler(handler);
            SimpleFormatter formatter = new SimpleFormatter();
            handler.setFormatter(formatter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("Pre-handle " + request + ": " + request.getMethod() + "\nURI: " + request.getRequestURI());
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        log.info("Post-handle " + request + ": " + request.getMethod() + "\nURI: " + request.getRequestURI());
        System.out.println("Post Handle was Called");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        if(ex!=null){
            log.info("Encountered error: " + ex.getMessage());
        }
        log.info("Request " + request + " completed");
    }

}
