package cz.cvut.fel.nss.model;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = "Team.findById", query ="SELECT t FROM Team t WHERE id = ?1")
})
public class Team extends AbstractEntity {
    @ManyToMany(mappedBy = "team")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<User> userList;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
}
