package cz.cvut.fel.nss;

import cz.cvut.fel.nss.model.*;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public class UserProxy {
    private User user;

    public UserProxy(User user) {
        this.user = user;
    }

    public void setRoles(Collection<Role> roles) {
        user.setRoles(roles);
    }

//    public Collection<Task> getParticipatingTasks() {
//        return user.getParticipatingTasks();
//    }
//
//    public void setParticipatingTasks(Collection<Task> participatingTasks) {
//      user.setParticipatingTasks(participatingTasks);
//    }


    public List<Task> getOwnedTasks() {
        return user.getOwnedTasks();
    }

    public void setOwnedTasks(List<Task> ownedTasks) {
       user.setOwnedTasks(ownedTasks);
    }

    public void setTeam(Collection<Team> team) {
       user.setTeam(team);
    }

    public Set<Task> getSubscribedTasks() {
      return user.getSubscribedTasks();
    }

    public void setSubscribedTasks(Set<Task> userTasks) {
        user.setSubscribedTasks(userTasks);
    }

    public List<ListOfItems> getUserLists() {
      return user.getUserLists();
    }

    public void setUserLists(List<ListOfItems> userLists) {
       user.setUserLists(userLists);
    }

    public int getId(){
        return user.getId();
    }

    public void setId(int id){
        user.setId(id);
    }

    public String getFirstName() {
        return user.getFirstName();
    }
    public String getPasswordConfirm() {
       return user.getPasswordConfirm();
    }
    public String getLastName() {
        return user.getLastName();
    }

    public String getUsername() {
        return user.getUsername();
    }

    public String getPassword() {
       return user.getPassword();
    }

    public int getExperienceRating(){
        return user.getExperienceRating();
    }

    public void setExperienceRating(int experienceRating){
        user.setExperienceRating(experienceRating);
    }
    public String getEmail() {
       return user.getEmail();
    }

    public int getPhone() {
        return user.getPhone();
    }

    public void setFirstName(String firstName) {
       user.setFirstName(firstName);
    }

    public void setLastName(String lastName) {
        user.setLastName(lastName);
    }

    public void setUsername(String username) {
       user.setUsername(username);
    }

    public void setPassword(String password) {
        user.setPassword(password);
    }

    public void setEmail(String email) {
      user.setEmail(email);
    }

    public void setPhone(int phone) {
       user.setPhone(phone);
    }
    public void setPasswordConfirm(String passwordConfirm) {
        user.setPasswordConfirm(passwordConfirm);
    }
    public Collection<Role> getRoles() {
      return user.getRoles();
    }

    public void setRoles(Set<Role> roles) {
       user.setRoles(roles);
    }

    public Collection<Team> getTeam() {
       return user.getTeam();
    }

    public void setTeam(List<Team> team) {
       user.setTeam(team);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
