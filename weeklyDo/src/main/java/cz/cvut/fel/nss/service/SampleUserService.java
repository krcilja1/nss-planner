package cz.cvut.fel.nss.service;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

@Service
public class SampleUserService {

    @Secured("ROLE_USER")
    public String sampleUserTask() {
        return "Hello Security";
    }
}
