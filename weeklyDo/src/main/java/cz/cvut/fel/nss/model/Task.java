package cz.cvut.fel.nss.model;

import cz.cvut.fel.nss.dto.TaskDto;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = "Task.findByDate", query ="SELECT t FROM Task t WHERE date = ?1"),
        @NamedQuery(name = "Task.findById", query ="SELECT t FROM Task t WHERE id = ?1")
})
public class Task extends AbstractEntity{
    private boolean closed;
    private String content;
    private Date createdAt;
    private Date deadline;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTags(Collection<Tag> tags) {
        this.tags = tags;
    }

    public boolean isClosed() {
        return closed;
    }

    public String getContent() {
        return content;
    }

    public Date getCreatedAtDate() {
        return createdAt;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setCreatedAtDate(Date date) {

        this.createdAt = date;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_task_owned")
    private User owner;

   @ManyToMany(fetch = FetchType.EAGER,mappedBy = "subscribedTasks")
  // @JoinTable(name = "user_task")
    private List<User> participants;


   public TaskDto convertToDto(){
       TaskDto result = new TaskDto();
       result.setId(this.getId());
       result.setClosed(this.closed);
       result.setContent(this.content);
       result.setDeadline(String.valueOf(this.deadline));
       result.setName(this.name);
       return result;
   }


    @ManyToMany()
    @JoinTable(
            name = "tasks_tags",
            joinColumns = @JoinColumn(
                    name = "task_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "tag_id", referencedColumnName = "id"))
    @LazyCollection(LazyCollectionOption.FALSE)
    private Collection<Tag> tags;

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User user) {
        this.owner = user;
    }


    public void setTags(List<Tag> taskTags) {
        this.tags = taskTags;
    }

    public Collection<Tag> getTags() {
        return tags;
    }


    public List<User> getParticipants() {
        return participants;
    }

    public void setParticipants(List<User> participants) {
        this.participants = participants;
    }
}
