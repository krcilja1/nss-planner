package cz.cvut.fel.nss.dao;

import cz.cvut.fel.nss.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleDao extends JpaRepository<Role, Long> {

    List<Role> findByName(String name);
}
