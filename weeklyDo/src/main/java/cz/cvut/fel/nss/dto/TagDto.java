package cz.cvut.fel.nss.dto;

public class TagDto {
    private int id;
    private String name;

    public TagDto(){}

    public TagDto(String name) {
        this.name = name;
    }

    public TagDto(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
